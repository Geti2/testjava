package Main;

import java.util.Arrays;

public class SecondMethod {

    private int [] mas_left;
    private int [] mas_right;
    private int [] mas;

    private int [] mas_out;


    private int number_right = 0;
    private int number_left = 0;
    private int main_sum = 0;
    private int number = 0;

    SecondMethod(int[] mas, int number)
    {
        this.number = number;
        this.mas = new int[number];
        this.mas = mas;
    }


    public int findOptimumWeightS() {

        int [] mas1;
        mas_out = Arrays.copyOf(mas,number);

        mas_left = new int[number];
        mas_right = new int[number];


        int[] mas_checks_values_smole = new int[number];
        int number_checks_values_smole = 0;

        int mas_check_value[] = new int[number];
        int number_check_value = 0;

        //1
        for (int i = number - 1; i >= 0; i--) {
             int n = 0;
            boolean check = true;

            int s = i;


            while (number_check_value > 0) {
                number_check_value = DeleteValuesWithCheckMas(mas_check_value, number_check_value);
            }


            mas_check_value[number_check_value] = mas[s];
            number_check_value++;

            while (s>=0) {

                int z = s-1;

                n++;

                int ss = 0;
                while (z > -1) {


                 mas1 = Arrays.copyOf(mas,number);



                    int a = number_check_value;
                    int number1 = i;
                    while (a > 0) {
                        number1 =  DeleteValuesWithMas(mas1,mas_check_value[number_check_value-1],number1);
                         a--;
                    }

                    int sum_mas_check_value = SumValues(mas_check_value, number_check_value);

                    int k = number1;

                    //2
                    while (k >= 0) {
                        int p = k;
                        //3
                        while (p >= 0) {

                            int sum_mas_checks_values_smole = SumValues(mas_checks_values_smole, number_checks_values_smole);

                            if (sum_mas_checks_values_smole < sum_mas_check_value) {
                                mas_checks_values_smole[number_checks_values_smole] = mas1[p];
                                number_checks_values_smole++;
                                p--;
                            }

                            sum_mas_checks_values_smole = SumValues(mas_checks_values_smole, number_checks_values_smole);

                            if (sum_mas_checks_values_smole > sum_mas_check_value) {
                                number_checks_values_smole = DeleteValuesWithCheckMas(mas_checks_values_smole, number_checks_values_smole);

                            } else if (sum_mas_checks_values_smole == sum_mas_check_value) {

                                check = false;

                                while (number_check_value > 0) {
                                    mas_right[number_right] = mas_check_value[number_check_value - 1];
                                    number_right++;
                                    i = DeleteValuesWithMainMas(mas, mas_check_value[number_check_value - 1], i);
                                    number_check_value = DeleteValuesWithCheckMas(mas_check_value, number_check_value);
                                    int tt = 0;
                                }

                                while (number_checks_values_smole > 0) {
                                    mas_left[number_left] = mas_checks_values_smole[number_checks_values_smole - 1];
                                    number_left++;
                                    i = DeleteValuesWithMainMas(mas, mas_checks_values_smole[number_checks_values_smole - 1], i);
                                    number_checks_values_smole = DeleteValuesWithCheckMas(mas_checks_values_smole, number_checks_values_smole);
                                }
                                i++;
                                break;
                            }
                        }
                        if (check == false) {
                            break;
                        }
                        while (number_checks_values_smole > 0) {
                            number_checks_values_smole = DeleteValuesWithCheckMas(mas_checks_values_smole, number_checks_values_smole);
                        }
                        k--;
                    }

                    if (check == false) {
                        break;
                    }



                    if (number_check_value > n) {
                        number_check_value = DeleteValuesWithCheckMas(mas_check_value, number_check_value);
                    }

                    mas_check_value[number_check_value] = mas1[z];
                    number_check_value++;

                    z--;

                }
                if (check == false) {
                    break;
                }
                s--;
            }



        }
        return SumValues(mas_left,number_left);
    }



    //сложения всех чисел массива
    private  int SumValues(int[] mas, int number)
    {
        int sum = 0;
        for (int i = 0; i < number; i++)
        {
            sum = sum + mas[i];
        }

        return sum;

    }

    //вывод массива
    private void ShowValues(int[] mas,int  number)
    {
        System.out.print(" [ ");
        for (int i = 0; i < number; i++)
        {
            System.out.print(mas[i]+" ");
        }
        System.out.print("]");
    }







    //удаляем эллемент из масива
    private  int DeleteValuesWithCheckMas(int[]mas,int number)
    {
        mas[number-1] = 0;
        number --;
        return number;
    }

    //удаляем эллемент из масива
    private  int DeleteValuesWithMas(int[]arr,int value_delete,int nElems)
    {
        int j=0;
        for (j = 0; j < nElems; j++) {//поиск удаляемого элемента
        if (arr[j] == value_delete)
            break;
        }

        for (int k = j; k < nElems - 1; k++) //сдвиг последующих элементов
            arr[k] = arr[k + 1];
        nElems--;
       return nElems;
    }

    private  int DeleteValuesWithMainMas(int[]arr,int value_delete,int nElems)
    {
        int j=0;
        for (j = nElems; j >= 0; j--) {//поиск удаляемого элемента
            if (arr[j] == value_delete)
                break;
        }

        for (int k = j; k <= nElems - 1; k++) //сдвиг последующих элементов
            arr[k] = arr[k + 1];
        nElems--;
        return nElems;
    }




    public void answer()
    {
        int tt = number;

        main_sum = SumValues(mas_out,number);
        for(int i = 0; i < number_right; i++)
        {
            tt = DeleteValuesWithMas(mas_out,mas_right[i],tt);
        }
        for(int i = 0; i < number_left; i++)
        {
            tt = DeleteValuesWithMas(mas_out,mas_left[i],tt);
        }


       int sum_left = SumValues(mas_left,number_left);
       int sum_right = SumValues(mas_right,number_right);


       if(main_sum == sum_left+sum_right  )
       {
           System.out.print("Можно нацепить блины" );
           ShowValues(mas_left,number_left);
           System.out.println("с одной стороны и блины ");
           ShowValues(mas_right,number_right);
           System.out.println( " с другой, что в сумме даст  "+ main_sum);
       }
       else if(number_left == 0 || number_right == 0)
       {
           System.out.println("На выходе 0");
           System.out.println("Пояснение: данный набор блинов невозможно нацепить без потери равновесия.");
       }
       else if(sum_left == sum_right)
       {

           int sum_all = sum_left+sum_right;
           System.out.print("Можно нацепить блины" );
           ShowValues(mas_left,number_left);
           System.out.print(" с одной стороны и блины ");
           ShowValues(mas_right,number_right);
           System.out.println( " с другой, что в сумме даст  "+ sum_all +".");
           System.out.print("Оставшийся блин(ы)");
           ShowValues(mas_out,tt);
           System.out.println("не может(гут) быть нацеплен(ы) ни с одной из сторон без потери равновесия.");
       }


    }
}
