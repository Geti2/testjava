package Main;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int number = QuantityNumber();

        int mas[] = new int[number];

        System.out.println("Введите мыссы " + number + " блинов");
        AddValues(mas);

        /*Random random = new Random();
        for(int i = 0 ; i < number; i++) {
            int p = random.nextInt(21);
            mas[i] = p;
        }*/


        System.out.print("На входе: ");
        ShowValues(mas,number);
        System.out.println();

        int sum = SumValues(mas,number);
        if(sum>10000) {
            System.out.println("Сумарный вес больше 10000");
            System.exit(0);
        }

        SortingMass(mas,number);

        int mas1[] = new int[number];
        int mas2[] = new int[number];

        for(int i = 0 ; i< number ; i++)
        {
            mas1[i] = mas[i];
            mas2[i] = mas[i];
        }



        SecondMethod secondMethod = new SecondMethod(mas,number);

        int sum_secondMethod = secondMethod.findOptimumWeightS();

        FirstMethod firstMethod = new FirstMethod(mas1,number,sum);
        int sum_firstMethod  = firstMethod.findOptimumWeightF();


        if(sum_secondMethod > sum_firstMethod) {
            secondMethod.answer();
        }
        else {
            firstMethod.answer();
        }


    }


    //добавить (вручную) числа в массив
    private static void AddValues(int[] mas )
    {
        int value;
        Scanner scanner = new Scanner(System.in);

        try
        {
            for (int i = 0; i < mas.length; i++) {
                System.out.print(i+1+". ");
                value = scanner.nextInt();
                if(value >= 0 && value <= 20)
                {
                    mas[i] = value;
                }
                else
                {
                    System.out.println("Блин не может весить больше 20 или меньше 0.");
                    i--;
                }
            }
        }
        catch (Exception e)
        {
            System.out.println("Перепроверте, что вы указали целое число");
            System.exit(0);
        }
    }
//сложения всех чисел массива
private static int SumValues(int[] mas, int number)
    {
        int sum = 0;
          for (int i = 0; i < number; i++)
          {
                sum = sum + mas[i];
          }

            return sum;

    }

//вывод массива
private static void ShowValues(int[] mas,int  number)
    {
        System.out.print(" [ ");
        for (int i = 0; i < number; i++)
        {
            System.out.print(mas[i]+" ");
        }
        System.out.print("]");
    }

//сколько будет эллементов в массиве
private static int QuantityNumber()
    {
        int num = 0;
        try {
            while(num < 1 || num > 1000)
            {
                Scanner scanner = new Scanner(System.in);
                System.out.print("Укажите сколько блинов будет на входе: ");
                num = scanner.nextInt();
            }

        }
        catch (Exception e)
        {
            System.out.println("Перепроверте, что вы указали целое число");
            System.exit(0);
        }
        return  num;
    }


//сортировка массива
private static void SortingMass(int[] mas, int number)
    {
        for(int i = number-1 ; i > 0 ; i--)
        {
            for(int j = 0 ; j < i ; j++) {

                if (mas[j] > mas[j + 1]) {
                    int tmp = mas[j];
                    mas[j] = mas[j + 1];
                    mas[j + 1] = tmp;
                }
            }
        }
    }




}



